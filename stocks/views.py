from django.shortcuts import render, redirect
from .models import Stock
from .forms import StockForm
from django.contrib import messages

# region Page rendering
def home(request):
    """
    Home page request page render

    :param request:
    :return:
    """

    #
    if request.method == 'POST':
        stock = request.POST['stock_symbol']
        try:
            sec_data = pdr_wrapper(stock)
        except Exception as e:
            sec_data = {}
        return render(request, 'home.html',
                      {'sec_data': sec_data.to_dict('records')[0],
                       'stock': stock,})
    else:
        return render(request, 'home.html',
                      {'stock': 'Enter Stock Symbol...'})


def add_stock(request):
    """
    Add stock page render
    Made with pure requests and json wrapper

    :param request:
    :return:
    """
    import json
    import requests

    if request.method == 'POST':
        form = StockForm(request.POST or None)
        if form.is_valid():
            form.save()
            messages.success(request, ("Stock has been added"))
            return redirect('add_stock')
    else:
        symbol = Stock.objects.all()
        output = []
        for symbol_item in symbol:
            api_request = requests.get("https://cloud.iexapis.com/stable/stock/"+str(
                symbol_item)+"/quote?token=YOUR_TOKEN")
            try:
                api_request_content = json.loads(api_request.content)
                output.append(api_request_content)
            except Exception as e:
                api_request_content = 'Error...'
        return render(request, 'add_stock.html',
                      {'output': output,
                       'symbol': symbol})


def delete_stock(request):
    """
    Delete stock page work with builtin Django model


    :param request:
    :return:
    """
    symbol = Stock.objects.all()
    return render(request, 'delete_stock.html', {'symbol': symbol})


def about(request):
    return render(request, 'about.html', {})
# endregion


# region Utils
def pdr_wrapper(stock, data_source='yahoo'):
    """
    Do nothing
    # TODO Add functionality, token, iex, list of stocks

    :param stock:
    :param data_source:
    :return:
    """
    import pandas_datareader as pdr
    import requests_cache
    from datetime import datetime, timedelta

    # Init sqlite cache backend
    expire_after = timedelta(days=1)
    session = requests_cache.CachedSession(cache_name='cache', backend='sqlite',
                                           expire_after=float(expire_after))
    end_date = datetime.today().strftime('%Y %m %d')
    start_date = datetime.strftime(datetime.now() - timedelta(days=2), '%Y %m %d')
    return pdr.DataReader(stock,
                          data_source,
                          start=start_date,
                          end=end_date,
                          # end=datetime(2019, 7, 10),
                          session=session)


def delete(request, stock_id):
    """
    Delete objects from DB


    :param stock_id: Stock id
    :return: Redirect to rendered page
    """
    item = Stock.objects.get(pk=stock_id)
    item.delete()
    messages.success(request, ("Stock has been deleted!"))
    return redirect('delete_stock')
# endregion
